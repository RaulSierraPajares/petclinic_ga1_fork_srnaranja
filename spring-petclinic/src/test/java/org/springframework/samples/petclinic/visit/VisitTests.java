package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.visit.Visit;
public class VisitTests {
	
	public static Visit visit;
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		visit = new Visit();
	}

	@Test
	public void testVisit() {
		assertNotNull(visit);				
	}
	
	@Test
	public void testDate() {
		assertNotNull(visit.getDate());
		assertEquals(LocalDate.now(),visit.getDate());
		
		visit.setDate(LocalDate.of(1999, 9, 9));
		
		assertNotNull(visit.getDate());
		assertEquals(LocalDate.of(1999, 9, 9),visit.getDate());
	}
	
	@Test
	public void testDescription() {
		assertNull(visit.getDescription());
		
		visit.setDescription("Descripcion 1");
		assertNotNull(visit.getDescription());
		assertEquals("Descripcion 1",visit.getDescription());

	}
	
	@Test
	public void testId() {
		assertNull(visit.getId());
		visit.setId(1);
		assertNotNull(visit.getId());
		
		Integer i = new Integer(1);
		assertEquals(i,visit.getId());
	}
	
	@Test
	public void testPetId() {
		assertNull(visit.getPetId());
		visit.setPetId(1);
		assertNotNull(visit.getPetId());
		
		Integer i = new Integer(1);

		assertEquals(i,visit.getPetId());
	}
	
	@Test
	public void testVet() {
		
		assertNull(visit.getVet());
		
		Vet vet = new Vet();
		vet.setFirstName("Juan Manuel");
		vet.setLastName("Murillo");		
		visit.setVet(vet);
		
		assertNotNull(visit.getVet());
		assertEquals(vet,visit.getVet());
	}
	@AfterClass
    //This method is executed after all the tests included in this suite are completed.
    public static void finishClass() {
		visit = null;
    }
	
}
