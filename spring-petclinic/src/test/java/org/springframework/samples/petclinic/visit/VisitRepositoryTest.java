package org.springframework.samples.petclinic.visit;
import static org.junit.Assert.*; 
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTest {
	
	@Autowired
	public  VisitRepository visits;
	
	@Autowired
	public  VetRepository vets;
	
	@Autowired
	public  PetRepository pets;
	
	@Autowired
	public  OwnerRepository owners;
	
	public Visit visit;
	
	@Before
	public void init() {
		if(visit == null) {
			visit = new Visit();
			visit.setDescription("Esta es una visita para probar los test del repositorio");
			PetType petType = this.pets.findPetTypes().get(0);
			Pet pet = new Pet();
			pet.setType(petType);
			pet.setName("PruebaPet");
			
			Owner owner = new Owner();
			owner.setFirstName("PruebaOwner");
			owner.setLastName("OwnerPruebaLastName");
			owner.setAddress("PruebaCalle");
			owner.setCity("PruebaCiudad");
			owner.setTelephone("654321987");
			this.owners.save(owner);
			
			owner.addPet(pet);
			
			this.pets.save(pet);
			
			Vet vet = new Vet();
			vet.setFirstName("Prueba");
			vet.setLastName("PruebaLastName");
			vet.setHomeVisits(false);
			vets.save(vet);
			visit.setVet(vet);
			visit.setPetId(pet.getId());
			visits.save(visit);
		}
	} 
	
	@Test
	public void testFindById() {
		Visit visitFindById = this.visits.findById(visit.getId());
		
		assertNotNull(visitFindById);
		assertNotNull(visitFindById.getDescription());
		assertEquals(visitFindById.getDescription(), this.visit.getDescription()); 
		
		assertNotNull(visitFindById.getDate());
		assertEquals(visitFindById.getDate(), this.visit.getDate());
		
		assertNotNull(visitFindById.getId());
		assertEquals(visitFindById.getId(), this.visit.getId());
		
		assertNotNull(visitFindById.getPetId());
		assertEquals(visitFindById.getPetId(), this.visit.getPetId());
		
		assertNotNull(visitFindById.getVet());
		assertEquals(visitFindById.getVet(), this.visit.getVet());
	}
	
	@Test
	public void testFindByIdNotEqual() {
		Visit visitSave = new Visit();
		visitSave.setDescription("Esta es una nueva visita para probar el repositorio");
		PetType petType = this.pets.findPetTypes().get(0);
		Pet pet = new Pet();
		pet.setType(petType);
		pet.setName("SecondPruebaPet");
		
		Owner owner = new Owner();
		owner.setFirstName("SecondPruebaOwner");
		owner.setLastName("SecondOwnerPruebaLastName");
		owner.setAddress("SecondPruebaCalle");
		owner.setCity("SecondPruebaCiudad");
		owner.setTelephone("654321987");
		this.owners.save(owner);
		
		owner.addPet(pet);
		
		this.pets.save(pet);
		
		
		Vet vet = new Vet();
		vet.setFirstName("SecondPrueba");
		vet.setLastName("SecondPruebaLastName");
		vet.setHomeVisits(false);
		this.vets.save(vet);
		visitSave.setVet(vet);
		visitSave.setPetId(pet.getId());
		
		assertNull(visitSave.getId());
		
		this.visits.save(visitSave);
		
		assertNotNull(visit.getId());
		
		Visit visitFindById = this.visits.findById(visit.getId());
		
		assertNotNull(visitFindById.getDate());
		
		assertNotNull(visitFindById.getDescription());
		assertNotEquals(visitFindById.getDescription(), visitSave.getDescription());	
		assertNotEquals(visitFindById.getId(), visitSave.getId());		
	}


}
